Feature: Create Lead in the application

Background:
Given Launch the browser
And Maximize the browser
And Wait
And Load the URL
And Enter the UserName
And Enter the Password
And Click on Login
And Click on crm/sfa
And Click on Leads tab

@smoke
Scenario Outline: Positive and Negative test
And Click on Create Lead link
And Enter Company Name as <companyName>
And Enter First name as <firstName>
And Enter Second name as <secondName>
When Click on Create lead button
Then Verify lead creation
And Close Browsers


Examples:
|companyName|firstName|secondName|
|TestLeaf|Viruksha|V|
|TLeaf|Vaibhav|V|
|CTS|Vaibhav|Vivek|