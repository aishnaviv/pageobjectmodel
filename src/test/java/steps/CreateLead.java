package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLead {
	ChromeDriver driver ;
	@Given("Launch the browser")
	public void launchTheBrowser() {
	    
		System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
		//instantiate chrome object
		driver = new ChromeDriver();
	}

	@Given("Maximize the browser")
	public void maximizeTheBrowser() {
	    
		driver.manage().window().maximize();
	}
	
	@Given("Wait")
	public void setTimeOuts() {
	    
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Given("Load the URL")
	public void loadTheURL() {
	    
		driver.get("http://leaftaps.com/opentaps/");
	}

	@Given("Enter the UserName")
	public void enterTheUserName() {
	    
		driver.findElementById("username").sendKeys("DemoSalesManager");
	}

	@Given("Enter the Password")
	public void enterThePassword() {
	    
		driver.findElementById("password").sendKeys("crmsfa");
	}

	@Given("Click on Login")
	public void clickOnLogin() {
	    
		driver.findElementByClassName("decorativeSubmit").click();
	}

	@Given("Click on crm\\/sfa")
	public void clickOnCrmSfa() {
	    
		driver.findElementByLinkText("CRM/SFA").click();
	}

	@Given("Click on Leads tab")
	public void clickOnLeadsTab() {
	    
		driver.findElementByXPath("//a[@href='/crmsfa/control/leadsMain']").click();		
	}

	@Given("Click on Create Lead link")
	public void clickOnCreateLeadLink() {
	    
		driver.findElementByLinkText("Create Lead").click();
	}

	@Given("Enter Company Name as (.*)")
	public void enterCompanyName(String cname) {
	    
		driver.findElementById("createLeadForm_companyName").sendKeys(cname);
		
	}

	@Given("Enter First name as (.*)")
	public void enterFirstName(String fname) {
	    
		driver.findElementById("createLeadForm_firstName").sendKeys(fname);
		
	}

	@Given("Enter Second name as(.*)")
	public void enterSecondName(String sname) {
		driver.findElementById("createLeadForm_lastName").sendKeys(sname);
	    
	}

	@When("Click on Create lead button")
	public void clickOnCreateLeadButton() {
	    
		driver.findElementByClassName("smallSubmit").click();
	}

	@Then("Verify lead creation")
	public void verifyLeadCreation() {
	    
		WebElement title = driver.findElementByXPath("//div[@class='frameSectionExtra']/preceding-sibling::div");
		String strTitle = title.getText();		
		String pageTitle = "View Lead";
		if (pageTitle.equals(strTitle))
		{
			System.out.println("Page title appeared is: "+strTitle+" as expected: "+pageTitle);
		}
		else
		{
			System.out.println("Page title is not correct. Hence lead is not created properly "+strTitle);
		} 
	}
	
	@Then("Close Browsers")
	public void closeBrowsers() {
	    driver.close();
	}

}