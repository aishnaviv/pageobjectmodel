package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC0003_MergeLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC003_MergeLead";
		testDescription = "MergeLead";
		testNodes = "Leads";
		authors ="Vaishnavi";
		category="smoke";
		dataSheetName = "TC001";
	}
	@Test(dataProvider="fetchData")
	public void logInLogOut(String uname, String pwd) throws InterruptedException {
		new LoginPage()
		.typeUsername(uname)
		.typePassword(pwd)
		.clickLogin()
		.clickCRM()
		.clickLeadButton()
		.clickMergeLeads()
		.selFromLead()
		.enterFirstName()
		.clickFindLeads()
		.selFromId()
		.selToLead()
		.enterFirstName()
		.clickFindLeads()
		.selToId().clickMerge();
		System.out.println("test");


	}

	
	

}
