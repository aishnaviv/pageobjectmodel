package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_ClickCRMSFA extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_ClickCRMSFA";
		testDescription = "Click CRM/SFA";
		testNodes = "Leads";
		authors ="Vaishnavi";
		category="smoke";
		dataSheetName = "TC001";
	}

	@Test(dataProvider="fetchData")
	public void logInLogOut(String uname, String pwd) throws InterruptedException {
		new LoginPage()
		.typeUsername(uname)
		.typePassword(pwd)
		.clickLogin()
		.clickCRM()
		.clickCreateLead()
		.enterCompanyNmae()
		.enterFirstName()
		.enterLastName()
		.clickCreateLead()
		.verifyFirstName();

	}

}
