package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC001_LogInLogout extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_LogInLogout";
		testDescription = "Login into leaftaps";
		testNodes = "Leads";
		authors ="Gayatri";
		category="smoke";
		dataSheetName = "TC001";
	}

	@Test(dataProvider="fetchData")
	public void logInLogOut(String uname, String pwd) throws InterruptedException {
		new LoginPage()
		.typeUsername(uname)
		.typePassword(pwd)
		.clickLogin();
		//.clickLogOut();

	}
}









