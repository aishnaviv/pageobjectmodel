package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeLead extends ProjectMethods{
	
	public MergeLead() {
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(xpath="//input[@name='partyIdFrom']/following::a") WebElement searchFromLead;
	public PopUpFindLeads selFromLead() throws InterruptedException {
		click(searchFromLead);
		switchToWindow(1);
		Thread.sleep(3000);
		return new PopUpFindLeads();		
		
	}
	@FindBy(xpath="//input[@id='partyIdTo']/following::a/img") WebElement searchToLead;
	public PopUpFindLeads selToLead() throws InterruptedException {
		click(searchToLead);
		switchToWindow(1);
		Thread.sleep(3000);
		return new PopUpFindLeads();			
	}
	@FindBy(xpath="//a[@class='buttonDangerous']")  WebElement clickMerge;
	public ViewLead clickMerge() {
		click(clickMerge);
		acceptAlert();
		return new ViewLead();
		
		
	}
}
