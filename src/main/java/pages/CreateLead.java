package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods {
	
	public CreateLead() {
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "(//input[@name=\"companyName\"])[2]")WebElement cmpyname;
	public CreateLead enterCompanyNmae() {
		type(cmpyname,"TestLeaf");
		return this;
	}		
	@FindBy(xpath="(//input[@name=\"firstName\"])[3]") WebElement firstname;
	public CreateLead enterFirstName() {
	type(firstname,"Viruksha");
	return this;
	}
			
	@FindBy(xpath="(//input[@name=\"lastName\"])[3]") WebElement lastName;
	public CreateLead enterLastName() {
		type(lastName, "Viruksha");
		return this;
	}
	
	@FindBy(name="submitButton") WebElement submitbutton;
	public VerifictionPage clickCreateLead()
	{
		click(submitbutton);
		return new VerifictionPage();
	}
	}

