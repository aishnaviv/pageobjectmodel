package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods{
	public HomePage()
	{
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(linkText="Create Lead") WebElement createLeadButton;
	public CreateLead clickCreateLead() {
		click(createLeadButton);
	return new CreateLead();
	}

	@FindBy(xpath="//a[text()='Leads']") WebElement clickLeads;
	public MyLeads clickLeadButton() {
		click(clickLeads);
		return new MyLeads();
		
	}
}
