package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import wdMethods.ProjectMethods;



public class VerifictionPage extends ProjectMethods {
	public VerifictionPage() {
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy( id="viewLead_firstName_sp") WebElement viewLead;
	public VerifictionPage verifyFirstName() {
		String text1 =getText(viewLead);
		System.out.println(text1);
		return this;
		
		
		
	}

}
