package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class LogOut extends ProjectMethods{

	
	
	public LoginPage clickLogOut() {
		WebElement eleLogOut = locateElement("class", "decorativeSubmit");
	    click(eleLogOut);  
	    return new LoginPage();
	}
	
}







