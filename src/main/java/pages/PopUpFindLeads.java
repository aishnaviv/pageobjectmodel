package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.SeMethods;

public class PopUpFindLeads extends SeMethods{
	
	public PopUpFindLeads() {
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(name="firstName")  WebElement  enterFirstName;	
	public PopUpFindLeads enterFirstName() {
		type(enterFirstName,"Viruksha");
		return this;
		
	}
	
	@FindBy(xpath="//button[@class='x-btn-text']")  WebElement  clickFindLeads;	
	public PopUpFindLeads clickFindLeads() throws InterruptedException {
		click(clickFindLeads);
		Thread.sleep(3000);
		return this;
		
	}
	
	@FindBy(xpath="(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a") WebElement selFromId;	
	public MergeLead selFromId() {
		click(selFromId);
		switchToWindow(0);
		return  new MergeLead();
		
	}
	
	@FindBy(xpath="(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[2]/a") WebElement selToId;	
	public MergeLead selToId() throws InterruptedException {
		click(selToId);
		switchToWindow(0);
		Thread.sleep(3000);
		return  new MergeLead();
		
	}
}
