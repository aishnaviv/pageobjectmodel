package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CRMPage extends ProjectMethods {
	

	public CRMPage() {
		PageFactory.initElements(driver, this);
	} 
	
	
	@FindBy(xpath="//div[@id='label']/a") WebElement crm;
	public HomePage clickCRM()
	{
		//WebElement crm = locateElement("xpath", "//div[@id='label']/a");	
		click(crm);
		return new HomePage();
	}

}
