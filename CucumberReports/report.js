$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/java/feature/Login.feature");
formatter.feature({
  "name": "Create Lead in the application",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "Positive and Negative test",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "Click on Create Lead link",
  "keyword": "And "
});
formatter.step({
  "name": "Enter Company Name as \u003ccompanyName\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "Enter First name as \u003cfirstName\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "Enter Second name as \u003csecondName\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "Click on Create lead button",
  "keyword": "When "
});
formatter.step({
  "name": "Verify lead creation",
  "keyword": "Then "
});
formatter.step({
  "name": "Close Browsers",
  "keyword": "And "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "companyName",
        "firstName",
        "secondName"
      ]
    },
    {
      "cells": [
        "TestLeaf",
        "Viruksha",
        "V"
      ]
    },
    {
      "cells": [
        "TLeaf",
        "Vaibhav",
        "V"
      ]
    },
    {
      "cells": [
        "CTS",
        "Vaibhav",
        "Vivek"
      ]
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "Launch the browser",
  "keyword": "Given "
});
formatter.match({
  "location": "CreateLead.launchTheBrowser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Maximize the browser",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.maximizeTheBrowser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Wait",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.setTimeOuts()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Load the URL",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.loadTheURL()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Enter the UserName",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.enterTheUserName()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Enter the Password",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.enterThePassword()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Click on Login",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.clickOnLogin()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Click on crm/sfa",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.clickOnCrmSfa()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Click on Leads tab",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.clickOnLeadsTab()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Positive and Negative test",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "Click on Create Lead link",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.clickOnCreateLeadLink()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Enter Company Name as TestLeaf",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.enterCompanyName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Enter First name as Viruksha",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.enterFirstName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Enter Second name as V",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.enterSecondName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Click on Create lead button",
  "keyword": "When "
});
formatter.match({
  "location": "CreateLead.clickOnCreateLeadButton()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Verify lead creation",
  "keyword": "Then "
});
formatter.match({
  "location": "CreateLead.verifyLeadCreation()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Close Browsers",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.closeBrowsers()"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "Launch the browser",
  "keyword": "Given "
});
formatter.match({
  "location": "CreateLead.launchTheBrowser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Maximize the browser",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.maximizeTheBrowser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Wait",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.setTimeOuts()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Load the URL",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.loadTheURL()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Enter the UserName",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.enterTheUserName()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Enter the Password",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.enterThePassword()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Click on Login",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.clickOnLogin()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Click on crm/sfa",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.clickOnCrmSfa()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Click on Leads tab",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.clickOnLeadsTab()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Positive and Negative test",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "Click on Create Lead link",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.clickOnCreateLeadLink()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Enter Company Name as TLeaf",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.enterCompanyName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Enter First name as Vaibhav",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.enterFirstName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Enter Second name as V",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.enterSecondName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Click on Create lead button",
  "keyword": "When "
});
formatter.match({
  "location": "CreateLead.clickOnCreateLeadButton()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Verify lead creation",
  "keyword": "Then "
});
formatter.match({
  "location": "CreateLead.verifyLeadCreation()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Close Browsers",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.closeBrowsers()"
});
formatter.result({
  "status": "passed"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.step({
  "name": "Launch the browser",
  "keyword": "Given "
});
formatter.match({
  "location": "CreateLead.launchTheBrowser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Maximize the browser",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.maximizeTheBrowser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Wait",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.setTimeOuts()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Load the URL",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.loadTheURL()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Enter the UserName",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.enterTheUserName()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Enter the Password",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.enterThePassword()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Click on Login",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.clickOnLogin()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Click on crm/sfa",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.clickOnCrmSfa()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Click on Leads tab",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.clickOnLeadsTab()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Positive and Negative test",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "name": "Click on Create Lead link",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.clickOnCreateLeadLink()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Enter Company Name as CTS",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.enterCompanyName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Enter First name as Vaibhav",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.enterFirstName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Enter Second name as Vivek",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.enterSecondName(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Click on Create lead button",
  "keyword": "When "
});
formatter.match({
  "location": "CreateLead.clickOnCreateLeadButton()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Verify lead creation",
  "keyword": "Then "
});
formatter.match({
  "location": "CreateLead.verifyLeadCreation()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Close Browsers",
  "keyword": "And "
});
formatter.match({
  "location": "CreateLead.closeBrowsers()"
});
formatter.result({
  "status": "passed"
});
});